//Enter your code here
#include<stdio.h>
int main()
{
  int i, sum=0;
  for (i=1;i<=10;i++)
   {
     sum=sum+i;
   }
  printf("The sum of the first 10 natural numbers is %d \n",sum);
  return 0;
}

//ODD NUMBERS 1-20
#include<stdio.h>
int main()
{
   int i;
   for(i=1;i<=20;i=i+2)
    {
      printf("%d \n",i);
    }
   return 0;
}

//TABLES OF 12
#include<stdio.h>
int main()
{
int i,p=1;
for(i=1;i<=10;i++)
{
p=12*i;
printf("\n 12*%d=%d",i,p);
}
return 0;
}


//REVERSE THREE DIGIT NUMBER
#include<stdio.h>
int main()
{
int n, r;
printf("Enter the number to be reversed \n");
scanf("%d",&n);
while(n!=0)
{
r=n%10;
n=n/10;
printf("%d",r);
}
return 0;
}
